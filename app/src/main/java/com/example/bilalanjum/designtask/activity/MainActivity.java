package com.example.bilalanjum.designtask.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.bilalanjum.designtask.R;
import com.example.bilalanjum.designtask.adapters.EmptyRecyclerViewAdapter;
import com.example.bilalanjum.designtask.adapters.InfoClassAdapter;
import com.example.bilalanjum.designtask.models.InfoClass;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<InfoClass> infolist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llm);


        infolist = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            InfoClass info = new InfoClass("Pakistan", "Hello to the citizens of Pakistan", R.drawable.btm_img, "3.3", "10 h ago", "10 miles away");
            infolist.add(info);
        }

        EmptyRecyclerViewAdapter mEmptyAdapter = new EmptyRecyclerViewAdapter(infolist);
        InfoClassAdapter ia = new InfoClassAdapter(infolist);

        if (infolist == null || infolist.size() == 0) {

            rv.setAdapter(mEmptyAdapter);
        } else {
            rv.setAdapter(ia);
        }


    }

}

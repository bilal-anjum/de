package com.example.bilalanjum.designtask.models;

/**
 * Created by Bilal Anjum on 8/1/2017.
 */

public class InfoClass {

    protected String title;
    protected String desciption;
    protected int image;
    protected String rating;
    protected String time;
    protected String distance;

    public InfoClass(String title, String desciption, int image,
                     String rating, String time, String distance) {
        this.title = title;
        this.desciption = desciption;
        this.image = image;
        this.rating = rating;
        this.time = time;
        this.distance = distance;
    }

    public String getTitle() {
        return title;
    }

    public String getDesciption() {
        return desciption;
    }

    public int getImage() {
        return image;
    }

    public  String getRating(){return rating;}

    public String getTime(){return time;}

    public String getDistance(){return distance;}
}
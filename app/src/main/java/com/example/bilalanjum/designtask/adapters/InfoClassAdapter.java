package com.example.bilalanjum.designtask.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bilalanjum.designtask.models.InfoClass;
import com.example.bilalanjum.designtask.R;

import java.util.List;

/**
 * Created by Bilal Anjum on 8/1/2017.
 */
public class InfoClassAdapter extends
        RecyclerView.Adapter<InfoClassAdapter.MyViewHolder> {

    private List<InfoClass> infolist;


    /**
     * View holder class
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titleText;
        public TextView descriptionText;
        public ImageView images;
        public TextView ratingText;
        public TextView timeText;
        public TextView distanceText;

        public MyViewHolder(View view) {
            super(view);
            titleText = (TextView) view.findViewById(R.id.title);
            descriptionText = (TextView) view.findViewById(R.id.description);
            images = (ImageView) view.findViewById(R.id.circle_image);
            ratingText = (TextView) view.findViewById(R.id.rating);
           // ratingText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tag_review, 0, 0, 0);
            timeText =  (TextView) view.findViewById(R.id.time);
            distanceText =  (TextView) view.findViewById(R.id.distance);
        }
    }

    public InfoClassAdapter(List<InfoClass> infolist) {
        this.infolist = infolist;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        InfoClass c = infolist.get(position);
        holder.titleText.setText(c.getTitle());
        holder.descriptionText.setText(c.getDesciption());
        holder.ratingText.setText(c.getRating());
        holder.timeText.setText(c.getTime());
        holder.distanceText.setText(c.getDistance());
    }

    @Override
    public int getItemCount() {
        return infolist.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);
        return new MyViewHolder(v);
    }
}
